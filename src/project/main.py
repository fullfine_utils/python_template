from dotenv import load_dotenv

load_dotenv()


def main():
    a = 2
    b = 3
    return a + b


if __name__ == "__main__":
    main()
